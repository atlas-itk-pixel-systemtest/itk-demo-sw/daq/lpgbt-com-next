from lpgbtcom import *

ctrl = FelixClientCtrl('lo', '../../bus', 'FELIX',
    rx_fid = 0x10000000001d0000,
    tx_fid = 0x1000000000118000,
    log_level = 3,
    verbose_bus = False)

ctrl.connect()


ic = ICMaster(ctrl)
ic.lpgbt_ver = 1
ic.i2c_addr = 0x74

# read ROM register
res = ic.read(0x1d7, 1)

print('reg: 0x%02X' % res[0])