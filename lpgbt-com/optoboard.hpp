#include "ic_master.hpp"

void config_lpgbt_master(ICManager &ic) {
  // for lpGBT v1

  uint16_t CLKGConfig = 0x020;
  ic.write(CLKGConfig, {
    0xe8, 0x38, 0x22, 0x99, 0x99, 0x55, 0x55, 0x66, 0x55, 0x1b, 0x00, 0x00, 0x88, 0x8f, 0xff,
    0x10, 0x10, 0x10 // FAMaxHeader...
  });

  uint16_t EPRXDllConfig = 0x0f1;
  ic.write(EPRXDllConfig, { 0x64, 0x2d, 0x05 });

  uint32_t LDConfigH = 0x039;
  ic.write(LDConfigH, { 0x7f });

  uint16_t EPRX0Control = 0x0c8;
  ic.write(EPRX0Control, { 0x1e, 0x1e, 0x1e, 0x1e, 0x1e, 0x1e });

  uint16_t EPRX00ChnCnt = 0x0d0;
  ic.write(EPRX00ChnCnt, {
    0x0a, 0x0, 0x0, 0x0,
    0x0a, 0x0, 0x0, 0x0,
    0x0a, 0x0, 0x0, 0x0,
    0x0a, 0x0, 0x0, 0x0,
    0x02, 0x0, 0x0, 0x0,
    0x02, 0x0, 0x0, 0x0,
  });

  uint16_t EQConfig = 0x037;
  ic.write(EQConfig, { 0x18 });

  uint16_t EPTXDataRate = 0x0a8;
  ic.write(EPTXDataRate, { 0xaa, 0x0, 0x55, 0x55 });

  uint16_t EPTX00ChnCntr = 0x0ae;
  ic.write(EPTX00ChnCntr, {
    0x03, 0x0, 0x03, 0x0, 0x03, 0x0, 0x03, 0x0, 0x03, 0x0, 0x03, 0x0, 0x03, 0x0, 0x03, 0x0, // EPTX..ChnCntr
    0x00, 0x00, 0x00, 0x00, 0x08, 0x08, 0x08, 0x08, // EPTX.._..CHNCNTR
  });

  uint16_t EPCLK5ChnCntrH = 0x078;
  ic.write(EPCLK5ChnCntrH, { 0x3e, 0x78 });

  uint16_t EPCLK2ChnCntrH = 0x072;
  ic.write(EPCLK2ChnCntrH, { 0x59, 0x00 });

  uint16_t EPCLK26ChnCntrH = 0x0a2;
  ic.write(EPCLK26ChnCntrH, { 0x59, 0x00 });

  uint16_t EPCLK19CHNCNTRH = 0x094;
  ic.write(EPCLK19CHNCNTRH, { 0x19, 0x00 });

  uint16_t POWERUP2 = 0x0fb;
  ic.write(POWERUP2, { 0x06 });

  uint16_t I2CMxConfig = 0x100;
  ic.write(I2CMxConfig, { 0x50 });

  // uint16_t RST0 = 0x13c;
  // ic.write(RST0, { 0x00 }); 
  // ic.write(RST0, { 0x04 }); // reset RSTi2cm0
  // ic.write(RST0, { 0x00 }); 

}



void config_lpgbt_slave(I2CMaster &i2c, uint16_t i2c_addr) {
  // for lpGBT v1

  uint16_t REFCLK = 0x03b;
  i2c.write2(i2c_addr, REFCLK, { 0x00 });

  uint16_t CLKGConfig = 0x020;
  i2c.write2(i2c_addr, CLKGConfig, {
    0x00, 0x38, 0x22, 0x99,
    0x99, 0x55, // missing CLKGPLLPROPCUR, CLKGCDRPROPCUR
    0x55, 0x66, 0x55, 0x1b, 0x00, 0x00, 0x88, 0x8f, 0xff
  });

  uint16_t PSDllConfig = 0x033;
  i2c.write2(i2c_addr, PSDllConfig, { 0x00 });

  uint16_t EPRXDllConfig = 0x0f1;
  i2c.write2(i2c_addr, EPRXDllConfig, { 0x64, 0x2d, 0x05 });

  uint32_t LDConfigH = 0x039;
  i2c.write2(i2c_addr, LDConfigH, { 0x7f });

  uint16_t EPRX0Control = 0x0c8;
  i2c.write2(i2c_addr, EPRX0Control, { 0x1e, 0x1e, 0x1e, 0x1e, 0x1e, 0x1e });

  uint16_t EPRX00ChnCnt = 0x0d0;
  i2c.write2(i2c_addr, EPRX00ChnCnt, {
    0x0a, 0x0, 0x0, 0x0,
    0x0a, 0x0, 0x0, 0x0,
    0x0a, 0x0, 0x0, 0x0,
    0x0a, 0x0, 0x0, 0x0,
    0x02, 0x0, 0x0, 0x0,
    0x02, 0x0, 0x0, 0x0,
  });

  uint16_t EPCLK5ChnCntrH = 0x078;
  i2c.write2(i2c_addr, EPCLK5ChnCntrH, { 0x3e, 0x78 });

  uint16_t POWERUP2 = 0x0fb;
  i2c.write2(i2c_addr, POWERUP2, { 0x06 });
}

void config_gbcr(I2CMaster &i2c, uint8_t i2c_addr) {
  i2c.write1(i2c_addr, 0, {
    0x3f, 0x88, 0x02, 0x3f, 0x88, 0x02, 0x3f, 0x88, 0x02, 0x3f, 0x88, 0x02, // CH.UPLINK
    0x3f, 0x88, 0x02, 0x3f, 0x88, 0x02, 0x3f, 0x88, 0x02,
    0x02, 0x1f, 0x55, 0x55, 0x55, 0x55, // PHASESHIFTER.
    0x70, // LVDSRXTX
    0x35, 0x01, 0x35, 0x01 // CH.DOWNLINK.
  });
}  

void printOptoboard(ICManager &ic) {
  printf("lpGBT master\n");
  
  ic.read(0, 0x150);
  auto reply = ic.wait_reply();
  auto res = ic.parse_reply(reply);
  printHex(res.data);

  I2CMaster i2c(ic, 0);
  i2c.reset();

  printf("lpGBT2\n");
  i2c.read2(117, 0, 0x150);

  printf("lpGBT3\n");
  i2c.read2(118, 0, 0x150);

  printf("lpGBT4\n");
  i2c.read2(119, 0, 0x150);

  printf("gbcr1\n");
  i2c.read2(32, 0, 0x20);

  printf("gbcr2\n");
  i2c.read2(33, 0, 0x20);

  printf("gbcr3\n");
  i2c.read2(34, 0, 0x20);

  printf("gbcr4\n");
  i2c.read2(35, 0, 0x20);

  printf("======\n");
}


void configureOptoboard(ICManager &ic) {
  printf("Configuring lpGBT1 (master)\n");
  config_lpgbt_master(ic);

  I2CMaster i2c(ic, 0);
  i2c.reset();
  printf("Configuring lpGBT2 (slave)\n");
  config_lpgbt_slave(i2c, 117);

  printf("Configuring lpGBT3 (slave)\n");
  config_lpgbt_slave(i2c, 118);

  printf("Configuring lpGBT4 (slave)\n");
  config_lpgbt_slave(i2c, 119);

  printf("Configuring gbcr1\n");
  config_gbcr(i2c, 32);

  printf("Configuring gbcr2\n");
  config_gbcr(i2c, 33);

  printf("Configuring gbcr3\n");
  config_gbcr(i2c, 34);

  printf("Configuring gbcr4\n");
  config_gbcr(i2c, 35);

  printOptoboard(ic);
}

