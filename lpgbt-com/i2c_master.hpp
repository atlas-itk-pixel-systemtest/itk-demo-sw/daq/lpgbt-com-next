#pragma once

class I2CMaster {

  enum Freq {
    Freq_100KHz = 0x0,
    Freq_200KHz = 0x1,
    Freq_400KHz = 0x2,
    Freq_1MHz   = 0x3
  };

  enum I2C_CMD {
    I2C_WRITE_CR        = 0x0,
    I2C_WRITE_MSK       = 0x1,
    I2C_1BYTE_WRITE     = 0x2,
    I2C_1BYTE_READ      = 0x3,
    I2C_1BYTE_WRITE_EXT = 0x4,
    I2C_1BYTE_READ_EXT  = 0x5,
    I2C_1BYTE_RMW_OR    = 0x6,
    I2C_1BYTE_RMW_XOR   = 0x7,
    I2C_W_MULTI_4BYTE0  = 0x8,
    I2C_W_MULTI_4BYTE1  = 0x9,
    I2C_W_MULTI_4BYTE2  = 0xA,
    I2C_W_MULTI_4BYTE3  = 0xB,
    I2C_WRITE_MULTI     = 0xC,
    I2C_READ_MULTI      = 0xD,
    I2C_WRITE_MULTI_EXT = 0xE,
    I2C_READ_MULTI_EXT  = 0xF,
  };

public:

  #define RegConfig   (BaseWrite + 0x00)
  #define RegAddress  (BaseWrite + 0x01)
  #define RegData0    (BaseWrite + 0x02)
  #define RegData1    (BaseWrite + 0x03)
  #define RegData2    (BaseWrite + 0x04)
  #define RegData3    (BaseWrite + 0x05)
  #define RegCmd      (BaseWrite + 0x06)

  #define RegCtrl     (BaseRead  + 0x00)
  #define RegMask     (BaseRead  + 0x01)
  #define RegStatus   (BaseRead  + 0x02)
  #define RegTranCnt  (BaseRead  + 0x03)
  #define RegReadByte (BaseRead  + 0x04)
  #define RegRead0    (BaseRead  + 0x05)
  #define RegRead1    (BaseRead  + 0x06)
  #define RegRead2    (BaseRead  + 0x07)
  #define RegRead3    (BaseRead  + 0x08)
  #define RegRead4    (BaseRead  + 0x09)
  #define RegRead5    (BaseRead  + 0x0a)
  #define RegRead6    (BaseRead  + 0x0b)
  #define RegRead7    (BaseRead  + 0x0c)
  #define RegRead8    (BaseRead  + 0x0d)
  #define RegRead9    (BaseRead  + 0x0e)
  #define RegRead10   (BaseRead  + 0x0f)
  #define RegRead11   (BaseRead  + 0x10)
  #define RegRead12   (BaseRead  + 0x11)
  #define RegRead13   (BaseRead  + 0x12)
  #define RegRead14   (BaseRead  + 0x13)
  #define RegRead15   (BaseRead  + 0x14)

  I2CMaster(ICMaster &ic_mstr, uint16_t master);
  void reset();

  uint8_t freq;

  uint8_t getCR_nbyte(uint8_t nbyte) {
    if (nbyte > 16) nbyte = 16;
    return (nbyte << 2) | freq;
  }
  void writeData(uint8_t val[16], uint8_t nbyte);
  void waitTransaction();
  void wait_i2c_success() {};
  void wait_transaction() {};
  void write_datapack() {};
  void get_cr_nbyte() {};
  void makeCtrlReg(uint16_t nbyte) {};
  void writeDataPack() {};
  void make_creg_nbyte(uint8_t nbyte) {};

  std::vector<uint8_t> read1(uint8_t i2c_addr, uint8_t mem_addr, uint8_t mem_size);
  std::vector<uint8_t> read2(uint8_t i2c_addr, uint16_t mem_addr, uint16_t mem_size);
  void write1(uint8_t i2c_addr, uint8_t addr, const std::vector<uint8_t> &data);
  void write2(uint8_t i2c_addr, uint16_t addr, const std::vector<uint8_t> &data);


private:
  uint16_t BaseWrite;
  uint16_t BaseRead;
  ICMaster &ic;
  uint8_t num;
  uint8_t SLCDriveMode;

};
