# lpgbt-com

lpGBT communication package

## clone orion-dist

```bash
git clone https://gitlab.cern.ch/itk-daq-sw/orion-dist.git
cd orion-dist
git submodule init
git submodule update
```

## clone lpgbt-com
```bash
git clone https://gitlab.cern.ch/itk-daq-sw/drivers/lpgbt-com.git
source ./setup.sh
```

## install felix-dist for corresponding $BINARY_TAG
```bash
source ./make_felix_dist.sh
source ${FELIX_PATH}/setup.sh
```

## compile lpgbt-com
```bash
cmake_config
cd ${BINARY_TAG}
make -j install
```

## test connection
To test lpgbt-com package, run python script from `python/test.py`.
Modify accordingly `bus_dir` property of `ctrl` controller.


