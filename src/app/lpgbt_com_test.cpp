
#include <assert.h>
#include <atomic>
#include <chrono>
#include <iostream>
#include <signal.h>
#include <string>
#include <thread>
#include <vector>

// // netio
// #include "netio/netio.hpp"

// // netio-next
// #include "netio/netio.h"
// #include "netio/netio_tcp.h"
#include "connect.hpp"
#include "ic_master.hpp"
#include "i2c_master.hpp"
//#include "optoboard.hpp"


//#include "felix/felix_client.hpp"

struct rxcfg_t {
  const char *localhost;
  const char *hostname;
  unsigned port;
  netio_tag_t tag;
};

struct txcfg_t {
  const char *hostname;
  unsigned port;
  netio_tag_t tag;
};

// struct {
//   const char* hostname;
//   unsigned port;
//   netio_tag_t tag;

//   //struct netio_timer subscribe_timer;
//   //int subrequest_sent;

//   //uint64_t expected_messages;
// } rxcfg;

// struct {
//   const char* hostname;
//   unsigned port;
//   netio_tag_t tag;

//   // struct netio_timer timer;
//   // const char* data;
//   // size_t len;
// } txcfg;


std::vector<uint8_t> read_config(std::string fname) {
  printf("fname = %s\n", fname.c_str());
  std::vector<uint8_t> data;

  std::ifstream f(fname);
  if (f.is_open()) {
    while (!f.eof()) {
      uint32_t u;
      f >> std::hex >> u;
      // printf("%02x ", u);
      data.push_back(u);
      // return data;
      // std::string str;
      // while (std::getline(f, str)) {
      //   uint32_t u;
      //   std::stringstream ss;
      //   ss << std::hex << str;
      //   ss >> u;
      //   printf("%s %08x\n", str.c_str(), u);
      //   //printf("%s\n", str.c_str());
    }
    f.close();
  } else {
    printf("cannot open file\n");
  }
  return data;
}

std::vector<uint8_t> get_config(std::vector<uint32_t> &data) {
  std::vector<uint8_t> cfg;
  for (auto u : data) {
    cfg.push_back(u >> 24);
    cfg.push_back(u >> 16);
    cfg.push_back(u >> 8);
    cfg.push_back(u >> 0);
  }
  return cfg;
}

void diff_cfg(std::vector<uint8_t> cfg1, std::vector<uint8_t> cfg2) {
  printf("=== diff: %ld - %ld ===\n", cfg1.size(), cfg2.size());
  uint16_t len = std::min(cfg1.size(), cfg2.size());
  for (int i = 0; i < len; i++) {
    if (cfg1[i] != cfg2[i]) {
      printf("reg = 0x%03x %02x %02x\n", i, cfg1[i], cfg2[i]);
    }
  }
}

Controller *ctrl = nullptr;

struct sigaction new_action, old_action;

void my_handler(int signum) {
  printf("received signal\n");

  if (ctrl)
    delete ctrl;
  exit(0);
  // sigaction(SIGINT, &old_action, NULL);
}

int main(int argc, char *argv[]) {

  // Add ctrl-c handler
  new_action.sa_handler = my_handler;
  sigemptyset(&new_action.sa_mask);
  new_action.sa_flags = 0;
  sigaction(SIGINT, NULL, &old_action);
  if (old_action.sa_handler != SIG_IGN) {
    sigaction(SIGINT, &new_action, NULL);
  }

  enum { stPixel, stStrips } setup;
  setup = stPixel;
  // setup = stStrips;

  txcfg_t txcfg;
  rxcfg_t rxcfg;

  const char *localhost = "128.141.204.161"; // "itkdell.dyndns.cern.ch"

  rxcfg.localhost = localhost;

  /*
    // pixel
    if (setup == stPixel) {
      rxcfg.remote_hostname = "pcatlitkflx01.cern.ch";
      rxcfg.port = 53100; // DAQ
      //rxcfg.port = 53300; // TTC
      //rxcfg.port = 53500; // DCS
      rxcfg.tag = 0x01d;

      txcfg.hostname = "pcatlitkflx01.cern.ch";
      txcfg.port = 53200;
      txcfg.tag  = 0x011;

      ic.i2c_addr = 0x74; // 116
      ic.lpGBT_ver = 1;
    }


    // strips
    if (setup == stStrips) {
      rxcfg.remote_hostname = "128.141.55.234"; //"pcatlitkflx03.cern.ch";
      //rxcfg.port = 53100; // DAQ d0
      //rxcfg.port = 53110; // DAQ d1
      //rxcfg.port = 53500; // DCS d0
      rxcfg.port = 53510; // DCS d1
      rxcfg.tag  = 0x15d; // d1
      //rxcfg.tag  = 0x15d; // d1
      //rxcfg.tag  = 0x0dd; // d0, ch 3

      //cfg.hostname = "pcatlitkflx03.cern.ch";
      txcfg.hostname = "128.141.55.234";
      //txcfg.port = 53200;
      txcfg.port = 53210; // d1
      txcfg.tag  = 0x155; //
      //txcfg.tag  = 0x015 + 0x40*3;

      //txcfg.tag  = 0x115 + 0x040*3;

      // 0x115 0x1000001001150000  (IC channel 4)
      // 0x155 0x1000001001550000  (IC channel 5)
      // 0x195 0x1000001001950000  (IC channel 6)
      // 0x1D5 0x1000001001d50000  (IC channel 7)



      ic.i2c_addr = 0x71; // 113
      ic.lpGBT_ver = 0;
    }
  */

  const char *flx01 = "128.141.55.235"; // pcatlitkflx01.cern.ch
  const char *flx03 = "128.141.55.234"; // pcatlitkflx03.cern.ch
  // const char* hostname = flx01;
  const char *hostname = flx03;
  // const char* hostname = "itkdell.cern.ch";

  // pixel multi-module setup
  txcfg.hostname = hostname;
  txcfg.port = 12340;
  txcfg.tag = 0x11; // 17

  rxcfg.hostname = hostname;
  rxcfg.port = 12350;
  rxcfg.tag = 0x1d; // 29

  // ctrl = new NetioClient(
  //   rxcfg.hostname, rxcfg.port, rxcfg.tag,
  //   txcfg.hostname, txcfg.port, txcfg.tag);

  rxcfg.hostname = flx01;
  // rxcfg.port = 53100; // DAQ
  // rxcfg.port = 53300; // TTC
  rxcfg.port = 53500;             // DCS
  rxcfg.tag = 0x10000000001d0000; // = 0x01d;

  txcfg.hostname = "pcatlitkflx01";
  txcfg.port = 53200;
  txcfg.tag = 0x11;

  rxcfg.hostname = hostname;
  // rxcfg.port = 53100; // DAQ
  // rxcfg.port = 53300; // TTC
  rxcfg.port = 53500;             // DCS
  rxcfg.tag = 0x10000010009d0000; // = 0x01d;

  txcfg.hostname = hostname;
  txcfg.port = 53200;
  txcfg.tag = 0x1000001000950000;

  //ctrl =
  //    new NetioNextClient(rxcfg.localhost, rxcfg.hostname, rxcfg.port,
  //                          rxcfg.tag, txcfg.hostname, txcfg.port, txcfg.tag);

  // FelixClient fc(rxcfg.localhost, "/home/gelix/ITk/itk-daq-sw/bus");
  // fc.subscribe(rxcfg.tag);

  // pixel
  txcfg.tag = 0x1000000000118000;
  rxcfg.tag = 0x10000000001d0000;

  // strips
  txcfg.tag = 0x1000000000158000;
  rxcfg.tag = 0x10000000001d0000;

  //std::string bus_dir = "/home/gelix/ITk/readout/bus_flx01";
  std::string bus_dir = "/home/gelix/ITk/readout/bus_flx05";
  ctrl = new FelixClientCtrl("em1", bus_dir, "FELIX", rxcfg.tag, txcfg.tag);

  ICMaster ic(*ctrl);

  // pixel
  ic.i2c_addr = 0x74;
  ic.lpgbt_ver = 1;

  // strips
  ic.i2c_addr = 0x71;
  ic.lpgbt_ver = 0;

  // printf(">>> delay ...\n");
  // std::this_thread::sleep_for(std::chrono::seconds(5));
  // printf(">>> done.\n");

  ctrl->connect();

  // return 0;

  uint16_t ROM = 0x1c5;
  if (ic.lpgbt_ver == 1)
    ROM = 0x1d7;

  /*
    ic.read(ROM, 1);

    if (comm.wait_reply()) {
      ic.printPacket(reply);
      print_hex(ic.getPacketData(reply));
    } else {
      printf("timeout\n");
    }
  */

  /*
    while (true) {

      std::this_thread::sleep_for(std::chrono::seconds(1));
      ic.read(ROM, 1);

      //flag = true;
      auto reply = ic.wait_reply();
      //print_hex(reply);
      auto res = ic.parse_reply(reply);
      print_hex(res.data);
      printf("\n");
    }
  */
  ic.read(ROM, 1);
  printf("Reg[ROM]\n");
  auto reply = ic.wait_reply();
  auto res = ic.parse_reply(reply);
  print_hex(res.data);
  printf("\n");

  return 0;

  I2CMaster i2c(ic, 0);
  i2c.reset();

  // printf("lpGBT2 ROM\n");
  // i2c.read2(0x75, ROM, 1);

  printf("configuring optoboard ...\n");
  // configureOptoboard(ic);
  printf("configured.\n");

  /*
    while (true) {
      std::cout << "." << std::flush;
      ic.read(0, 0x150);
      auto reply = ic.wait_reply();
      auto res = ic.parse_reply(reply);
      print_hex(res.data);
      std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }
  */

  /*
    ic.read(0, 0x150);
    comm.wait_reply();
    auto cfg1 = ic.getPacketData(reply);
    print_hex(cfg1);

    printf("Configuring lpGBT1 (master)\n");
    config_lpgbt_master(ic);

    ic.read(0, 0x150);
    comm.wait_reply();
    auto cfg2 = ic.getPacketData(reply);
    print_hex(cfg2);

    diff_cfg(cfg1, cfg2);


    I2CMaster i2c(ic, 0);

    for (uint8_t i2c_addr = 117; i2c_addr <= 119; i2c_addr++) {
      auto res1 = i2c.read2(i2c_addr, 0, 0x150);
      config_lpgbt_slave(i2c, i2c_addr);
      auto res2 = i2c.read2(i2c_addr, 0, 0x150);
      diff_cfg(res1, res2);
    }

    for (uint8_t i2c_addr = 32; i2c_addr <= 35; i2c_addr++) {
      auto res1 = i2c.read1(i2c_addr, 0, 0x20);
      config_gbcr(i2c, i2c_addr);
      auto res2 = i2c.read1(i2c_addr, 0, 0x20);
      diff_cfg(res1, res2);
    }
  */

  std::this_thread::sleep_for(std::chrono::seconds(1));
  // ctrl->disconnect();

  delete ctrl;

  return 0;

  // printf("Configuring lpGBT1 (master)\n");
  // config_lpgbt_master(ic);

  // uint16_t ROM = 0x1c5;
  // if (ic.lpGBT_ver == 1) ROM = 0x1d7;
  // ic.read(ROM, 1);

  // printf("configuring optoboard ...\n");
  // configureOptoboard(ic);
  // printf("configured.\n");

  /*
    auto cfg_pri =
  read_config("/home/gelix/ITk/strip/lpgbt_SS_S_LH_Pri_test1.cnf"); auto cfg_sec
  = read_config("/home/gelix/ITk/strip/lpgbt_SS_S_LH_Sec_test1.cnf");

    uint16_t k;
    k = 0x036;
    printf("reg[0x%03x] = 0x%02x\n", k, cfg_pri[k]);
    k = 0x0e8;
    printf("reg[0x%03x] = 0x%02x\n", k, cfg_pri[k]);
  // reg = 0x036 80 00
  // reg = 0x052 00 01
  // reg = 0x054 00 01
  // reg = 0x05a 00 01
  // reg = 0x0e8 01 09



    ic.write(0, cfg_pri);

    //uint16_t ROM = 0x1c5;
    //if (ic.lpGBT_ver == 1) ROM = 0x1d7;
    //ic.read(ROM, 1);

    //diff_cfg(cfg_pri, cfg_sec);
  */
  std::this_thread::sleep_for(std::chrono::seconds(2));

  printf("done.\n");

  return 0;
}
