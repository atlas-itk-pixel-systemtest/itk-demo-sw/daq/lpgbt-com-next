#include <stdint.h>
#include <thread>
#include <vector>
#include <assert.h>

#include "ic_master.hpp"

namespace {
  auto logger = make_log("ic.master");
}


void print_hex(std::vector<uint8_t> data) {

  uint k = 0;
  for (auto u : data) {
    //if (k % 4 == 0) printf("0x");
    if (k % 4 == 0) printf(" %03x: ", k);
    printf("%02x", u);
    k++;
    if (k % 32 == 0) printf("\n"); else
    //if (k % 4 == 0) printf(", ");
    if (k % 4 == 0) printf(" ");
  }
  printf("\n");
}


void print_bin(std::vector<uint8_t> data) {

  uint k = 0;
  for (auto u : data) {
    printf("%2d: %02x  ", k, u);
    std::cout << std::bitset<8>(u) << std::endl;
    k++;
  }
  printf("\n");
}


void ic_send(Controller &ctrl, uint8_t lpgbt_ver,
   uint16_t i2c_addr, uint16_t mem_addr, uint16_t mem_size, const uint8_t *data = nullptr)
{
  std::vector<uint8_t> buff;
  buff.reserve(mem_size + 8); // for lpgbt_v0

  bool read = (data == nullptr);

  if (lpgbt_ver == 0) buff.push_back(0); // reserved
  buff.push_back((i2c_addr << 1) | (read ? 0x1 : 0x0));
  buff.push_back(1); // not used, set 1 to avoid write if mix v0 and v1
  buff.push_back(mem_size & 0xFF); // number of bytes
  buff.push_back(mem_size >> 8);
  buff.push_back(mem_addr & 0xFF); // mmemory address
  buff.push_back(mem_addr >> 8);
  if (!read)
    buff.insert(buff.end(), data, data + mem_size);

  uint8_t parity = 0;
  uint16_t offset = (lpgbt_ver == 0 ? 2 : 0);
  for (auto i = offset; i < buff.size(); i++) parity ^= buff[i];
  buff.push_back(parity);

  // send to server
  ctrl.send_data(buff);
}


ICMaster::ICMaster() {
  lpgbt_ver = 0;
  i2c_addr = 0xFF;
}

ICMaster::ICMaster(Controller &ctrl) {
  this->ctrl = &ctrl;
  lpgbt_ver = 0;
  i2c_addr = 0xFF;
}


ICMaster::~ICMaster() {
}


void ICMaster::print_reply(std::vector<uint8_t> &msg) {
  auto res = parse_reply(msg);

  logger->info("i2c_addr: {}, addr: {x}, size: {}", res.i2c_addr, res.mem_addr, res.mem_size);
  if (res.status != 0) printf("error: status = %d\n", res.status);
  for (int i = 0; i < res.data.size(); i++) {
    logger->info("{:2d} : {:02x} | ", i, res.data[i]);
    std::cout << std::bitset<8>(res.data[i]) << std::endl;
  }
}


ICReply ICMaster::parse_reply(std::vector<uint8_t> &msg) {
  // ICReply.status
  // 0 - correct reply format
  // 1 - too short message
  // 2 - incorrect data size

  uint16_t size = msg.size();
  uint16_t idx = 0; // felix header already removed

  ICReply rep;
  rep.i2c_addr = 0;
  rep.mem_size = 0;
  rep.mem_addr = 0;
  rep.parity = 0;
  rep.status = 0; // correct reply format


  uint16_t min_size = idx + 6; // + ic reply header
  if (lpgbt_ver == 0) min_size += 1;
  min_size += 1; // + parity
  if (min_size > size) {
    rep.status = 0x1; // too short message
    return rep;
  }

  rep.i2c_addr = msg[idx + 0] >> 1;
  if (lpgbt_ver == 0) idx += 1;
  rep.command = msg[idx + 1];
  rep.mem_size = (msg[idx + 3] << 8) | msg[idx + 2];
  rep.mem_addr = (msg[idx + 5] << 8) | msg[idx + 4];
  rep.parity = msg[size - 1];

  idx += 6;
  rep.data.assign(msg.begin() + idx, msg.end() - 1);
  if (rep.mem_size != rep.data.size()) {
    rep.status = 0x2; // incorrect data size
  }

  return rep;
}


void ICMaster::send_write(uint16_t i2c_addr, uint16_t mem_addr, const uint8_t *data, uint16_t size) {
  ic_send(*ctrl, lpgbt_ver, i2c_addr, mem_addr, size, data);
}


void ICMaster::send_read(uint16_t i2c_addr, uint16_t mem_addr, uint16_t size) {
  ic_send(*ctrl, lpgbt_ver, i2c_addr, mem_addr, size);
}


ICReply ICMaster::send(uint16_t mem_addr, uint16_t mem_size, const uint8_t *data) {

  bool success = false;
  uint resend = 0;
  ICReply res;
  do {

    if (resend > 3) {
      logger->warn("too many resendings, exiting ...");
      return res;
    }
    if (resend) {
      // flush data before resending
      std::this_thread::sleep_for(std::chrono::milliseconds(resend_timeout));
      logger->warn("resending {} ...", resend);
    }
    resend++;

    // send to server
    ic_send(*ctrl, lpgbt_ver, i2c_addr, mem_addr, mem_size, data);

    ctrl->wait_reply(reply_timeout);
    res = parse_reply(ctrl->reply);

    if (res.status != 0) {
      if (ctrl->reply.size()) {
        logger->warn("cannot parse reply, resending ...");
        print_hex(ctrl->reply);
      } else {
        logger->warn("empty reply, resending ...");
      }
      continue;
    }
    if (res.i2c_addr != i2c_addr) {
      logger->warn("reply i2c_addr does not match, resending ...");
      continue;
    }
    if (res.mem_addr != mem_addr || res.mem_size != mem_size) {
      logger->warn("reply mem(addr, size) does not match, resending ...");
      continue;
    }
    if (data && !std::equal(data, data + mem_size, res.data.data())) {
      logger->warn("reply data does not match, resending ...");
      continue;
    }
    success = true;
  }
  while (!success);

  return res;
}
