#include <iostream>
#include "connect.hpp"


std::shared_ptr<spdlog::logger>
make_log(std::string logname) {
  if (auto existing_logger = spdlog::get(logname)) {

    return existing_logger;
  
  } else {

    auto new_logger = std::make_shared<spdlog::logger>(logname);
    spdlog::register_logger(new_logger);

    auto sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
    std::string log_pattern = "[%T:%e]%^[%=12l][%=15n]:%$ %v";
    new_logger->sinks().push_back(sink);
    new_logger->set_level(spdlog::level::debug);
    new_logger->set_pattern(log_pattern);

    return new_logger;
  }
}

/*
std::map<netio_tag_t, NetioCtrl*> netio_clients;

// Netio Communication
void NetioCtrl::connect() {
  logger->info("Netio connect | host: {}:{} | tx.tag: 0x{:x}", remotehost, tx.port, tx.tag);

  ctx = new netio::context("posix");
  ctx_thread = std::thread([&](){ ctx->event_loop()->run_forever(); });

  tx.sock = new netio::low_latency_send_socket(ctx);
  tx.ep = netio::endpoint(remotehost.c_str(), tx.port);
  tx.sock->connect(tx.ep);

  if (!tx.sock->is_open()) {
    logger->error("Unable to connect to FELIX endpoint");
    throw std::runtime_error("ICMaster: Unable to connect to FELIX endpoint.");
    abort();
  } else {
    logger->debug("Connected to FELIX endpoint");
  }

  netio::low_latency_recv_socket::callback_fn cb_msg_recv =
    std::bind(&NetioCtrl::on_msg_recv, this, std::placeholders::_1, std::placeholders::_2);

  rx.sock = new netio::low_latency_subscribe_socket(ctx, cb_msg_recv);
  rx.ep = netio::endpoint(remotehost.c_str(), rx.port);

  netio_clients[rx.tag] = this;
  rx.sock->subscribe(rx.tag, rx.ep);
  logger->info("  subscribed  | host: {}:{} | rx.tag: 0x{:x}", remotehost, rx.port, rx.tag);
}

void NetioCtrl::disconnect() {
  logger->info("Netio | disconnect");
  if (tx.sock) tx.sock->disconnect();

  if (rx.sock) {
    rx.sock->unsubscribe(rx.tag, rx.ep);
    netio_clients.erase(rx.tag);
  }
  if (ctx) ctx->event_loop()->stop();
  if (ctx_thread.joinable()) ctx_thread.join();
  if (ctx) delete ctx;
}

void NetioCtrl::send_data(std::vector<uint8_t> &data) {
  // send to FELIX
  ToFlxHeader header;
  header.length = data.size();
  header.reserved = 0;
  header.elink = tx.tag;

  netio::message msg;

  msg.add_fragment((uint8_t*)&header, sizeof(header));
  msg.add_fragment(data.data(), data.size());

  flag = true;
  reply.clear();
  tx.sock->send(msg);
}

void NetioCtrl::on_msg_recv(netio::endpoint& ep, netio::message& msg) {
  logger->debug("Received data from {}:{} size: {}", ep.address(), ep.port(), msg.size());

  if (!flag) {
    logger->warn("send flag is not set, ignoring message.");
    return;
  }

  reply = msg.data_copy();

  uint16_t header_size = sizeof(felix::base::FromFELIXHeader);
  if (reply.size() < header_size) {
    reply.clear();
    logger->warn("Too small Netio message.");
    return;
  }

  // auto &header = *(reinterpret_cast<felix::base::FromFELIXHeader*>(reply.data()));
  // uint32_t tag = header.gbtid*0x40 + header.elinkid;
  // auto search = netio_clients.find(tag);
  // if (search == netio_clients.end()) {
  //   printf("unknown tag = 0x%x, ignoring message.\n", tag);
  //   reply.clear();
  //   return;
  // }
  // auto &client = *search->second;

  reply.erase(reply.begin(), reply.begin() + header_size);
  logger->trace("reply size: {}", reply.size());
  flag = false;
}
*/
