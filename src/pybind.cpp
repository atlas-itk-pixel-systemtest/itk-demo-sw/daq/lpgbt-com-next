#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
// #include <pybind11/stl_bind.h>

#include <array>
#include <iostream>
#include <memory>
#include <vector>

#include "connect.hpp"
#include "ic_master.hpp"
#include "i2c_master.hpp"

namespace py = pybind11;

auto bytes_info(py::bytes &bytes) {
  struct {
    const uint8_t *buff;
    size_t size;
  } res;
  py::buffer_info info(py::buffer(bytes).request());
  res.buff = reinterpret_cast<const uint8_t*>(info.ptr);
  res.size = static_cast<size_t>(info.size);
  return res;
}


const std::vector<uint8_t> to_vector(py::bytes &bytes) {
  auto [buff, size] = bytes_info(bytes);
  const std::vector<uint8_t> data(buff, buff + size);
  return data;
}

const py::bytes to_bytes(const std::vector<uint8_t> &data) {
  const char *buff = reinterpret_cast<const char*>(data.data());
  size_t size = data.size();
  return py::bytes(buff, size);
}




PYBIND11_MODULE(lpgbtcom, m) {
  m.doc() = "Python bindings for Controller, ICMaster, I2CMaster";

  py::class_<Controller, std::shared_ptr<Controller>> controller(m, "Controller");
  controller
    .def_readwrite("reply", &Controller::reply)
    .def("connect", &Controller::connect)
    .def("disconnect", &Controller::disconnect)
    .def("send_data",
      [](Controller &self, py::bytes &bytes) {
        auto data = to_vector(bytes);
        self.send_data(data);
      }, py::arg("data"))
    .def("wait_reply", &Controller::wait_reply, py::arg("timeout") = 0);


  py::class_<FelixClientCtrl, std::shared_ptr<FelixClientCtrl>> fclient_ctrl(m, "FelixClientCtrl", controller);
  fclient_ctrl
    .def(py::init<std::string,
      std::string, std::string,
      netio_tag_t, netio_tag_t,
      int, bool>(),
      py::arg("localhost"),
      py::arg("bus_dir"), py::arg("bug_group"),
      py::arg("rx_fid"), py::arg("tx_fid"),
      py::arg("log_level"), py::arg("verbose_bus")
    );

  m.def("set_log_pattern",
    [](std::string pattern) {
      spdlog::apply_all([&](std::shared_ptr<spdlog::logger> logger) {
        logger->set_pattern(pattern);
      });
    }, py::arg("pattern"));

  m.def("set_log_level",
    [](uint level) {
      spdlog::apply_all([&](std::shared_ptr<spdlog::logger> logger) {
        logger->set_level(spdlog::level::level_enum(level));
      });
    }, py::arg("level"));


  py::class_<ICMaster, std::shared_ptr<ICMaster>> ic(m, "ICMaster");
  ic.def(py::init<Controller&>(), py::arg("client"))
    //.def_readwrite("client", &ICMaster::client)
    .def_readwrite("reply_timeout",  &ICMaster::reply_timeout)
    .def_readwrite("resend_timeout", &ICMaster::resend_timeout)
    .def_readwrite("lpgbt_ver", &ICMaster::lpgbt_ver)
    .def_readwrite("i2c_addr", &ICMaster::i2c_addr)
    .def("send_write",
      [](ICMaster &self, uint16_t i2c_addr, uint16_t mem_addr, py::bytes &bytes) {
        auto [data, size] = bytes_info(bytes);
        self.send_write(i2c_addr, mem_addr, data, size);
      },
      py::arg("i2c_addr"), py::arg("mem_addr"), py::arg("data"))
    .def("send_read", &ICMaster::send_read,
      py::arg("i2c_addr"), py::arg("mem_addr"), py::arg("data"))
    .def("read",
      [](ICMaster &self, uint16_t addr, uint16_t size) {
        return to_bytes(self.read(addr, size));
      }, py::arg("addr"), py::arg("size"))
    .def("write",
      [](ICMaster &self, uint16_t addr, py::bytes &bytes) {
        auto [data, size] = bytes_info(bytes);
        self.write(addr, data, size);
      }, py::arg("addr"), py::arg("data"))
    .def("wait_reply",
      [](ICMaster &self, uint32_t timeout) {
        return to_bytes(self.wait_reply(timeout));
      }, py::arg("timeout") = 0)
    .def("parse_reply",
      [](ICMaster &self, py::bytes &bytes) {
        auto data = to_vector(bytes);
        return self.parse_reply(data);
      }, py::arg("reply"));



  py::class_<I2CMaster, std::shared_ptr<I2CMaster>> i2c(m, "I2CMaster");
  i2c.def(py::init<ICMaster&, uint16_t>(), py::arg("ic"), py::arg("master"))
    .def("reset", &I2CMaster::reset)
    .def("read1",
      [](I2CMaster &self, uint16_t i2c_addr, uint8_t addr, uint8_t size) {
        return to_bytes(self.read1(i2c_addr, addr, size));
      }, py::arg("i2c_addr"), py::arg("addr"), py::arg("size"))
    .def("read2",
      [](I2CMaster &self, uint16_t i2c_addr, uint16_t addr, uint16_t size) {
        return to_bytes(self.read2(i2c_addr, addr, size));
      }, py::arg("i2c_addr"), py::arg("addr"), py::arg("size"))
    .def("write1",
      [](I2CMaster &self, uint16_t i2c_addr, uint8_t addr, py::bytes &bytes) {
        auto data = to_vector(bytes);
        self.write1(i2c_addr, addr, data);
      },
      py::arg("i2c_addr"), py::arg("addr"), py::arg("data"))
    .def("write2",
      [](I2CMaster &self, uint16_t i2c_addr, uint16_t addr, py::bytes &bytes) {
        auto data = to_vector(bytes);
        self.write2(i2c_addr, addr, data);
      },
      py::arg("i2c_addr"), py::arg("addr"), py::arg("data"))
    .def("wait_transaction", &I2CMaster::waitTransaction);
    // .def("wait_transaction", &ICMaster::wait_transaction, py::arg("timeout") = 3000);

    // struct ICReply
    py::class_<ICReply, std::shared_ptr<ICReply>> icreply(m, "ICReply");
    icreply.def(py::init<>())
      .def_readwrite("i2c_addr", &ICReply::i2c_addr)
      .def_readwrite("mem_addr", &ICReply::mem_addr)
      .def_readwrite("mem_size", &ICReply::mem_size)
      .def_readwrite("command", &ICReply::command)
      .def_readwrite("parity", &ICReply::parity)
      .def_readwrite("status", &ICReply::status)
      .def_readwrite("data", &ICReply::data);

  m.def("set_log_pattern",
    [](std::string pattern) {
      spdlog::apply_all([&](std::shared_ptr<spdlog::logger> logger) {
        logger->set_pattern(pattern);
      });
    }, py::arg("pattern"));

  m.def("set_log_level",
    [](uint level) {
      spdlog::apply_all([&](std::shared_ptr<spdlog::logger> logger) {
        logger->set_level(spdlog::level::level_enum(level));
      });
    }, py::arg("level"));
}
