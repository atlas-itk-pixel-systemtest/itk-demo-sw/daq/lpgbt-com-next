# download felix-distribution

BINARY_TAG="x86_64-el9-gcc13-opt"

FELIX_DIST=""

if [[ ${BINARY_TAG} == "x86_64-centos7-gcc11-opt" ]]
then
    FELIX_DIST="felix-05-00-02-rm5-stand-alone"
fi

if [[ ${BINARY_TAG} == "x86_64-el9-gcc13-opt" ]]
then
    FELIX_DIST="felix-05-00-03-rm5-stand-alone"
fi

if [[ ${FELIX_DIST} == "" ]]
then
    echo "Unknown BINARY_TAG: \"${BINARY_TAG}\""
    return 1
fi


FELIX_TARGZ=${FELIX_DIST}-${BINARY_TAG}.tar.gz
FELIX_PATH=${PWD}/${FELIX_DIST}/${BINARY_TAG}

wget https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/dist/software/apps/4.x/${FELIX_TARGZ}
tar -zxf ${FELIX_TARGZ}
wget https://gitlab.cern.ch/atlas-tdaq-felix/felix-client/-/raw/master/felix/felix_client_util.hpp -P ${FELIX_PATH}/include/felix

rm ${FELIX_TARGZ}

